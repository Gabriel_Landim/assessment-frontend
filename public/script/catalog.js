var searchIsOn = false

window.onload = async function () {
  let data = await getList()

  buildProducts(data)
};

async function buildProducts(data) {
  data.forEach(e => {
    const iten = document.createElement('div')
    let price = e.price

    price = transform(price)
    promo = transform(e.specialPrice)

    iten.innerHTML = `
              <img src="${e.image}" alt="">

              <p id="product-name">${e.name}</p>

              <div id="prices">
                <p>${price}</p>
                ${promo != undefined ? `<p>${promo}</p>` : ''}
              </div>

              <button id="buy">COMPRAR</button>
  `
    document.querySelector('#itens-list').appendChild(iten)
  });
}

async function getList(search, select) {
  let queryString = window.location.search;
  queryString = queryString.replace('?', '')

  let list, data
  await getData('list').then(res => {
    list = res.data.items
  })

  let categoria = list.find(elem => elem.path == queryString)

  document.getElementById('current-page').innerHTML = categoria.name
  document.getElementById('title').innerHTML = categoria.name

  await getData(categoria.id).then(res => {
    data = res.data.items
    if (search !== undefined) {
      data = data.filter( value => {
        return value.name.toLowerCase().includes(search.toLowerCase())
      })
    }

    if (select == 'preco') {
      data = data.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
    }
  })

  return data
}

async function getData(param) {
  const url = `http://localhost:8888/api/V1/categories/${param}`

  return await axios.get(url)
}

function transform(value) {
  if (value != undefined) {
    return (value.toLocaleString("pt-BR", {
      style: "currency",
      currency: "BRL"
    }))
  }
}

async function searchProduct() {
  const input = document.getElementById('input-search').value
  const select = document.getElementById('select-filter').value
  data = await getList(search = input, select)

  document.querySelector('#itens-list').innerHTML = ""

  buildProducts(data)
}

function openMenu() {
  document
    .querySelector('.menu-modal')
    .classList
    .add('active')
}

function closeMenu() {
  document
    .querySelector('.menu-modal')
    .classList
    .remove('active')
}

function openSearch() {
  if (searchIsOn == false) {
    searchIsOn = true

    document
      .querySelector('#search')
      .classList
      .add('active')
    
  } else {
    searchIsOn = false

    document
    .querySelector('#search')
    .classList
    .remove('active')
  }
}
