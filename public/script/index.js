var searchIsOn = false

function openMenu() {
  document
  .querySelector('.menu-modal')
  .classList
  .add('active')
}

function closeMenu(){
  document
  .querySelector('.menu-modal')
  .classList
  .remove('active')
}

function openSearch() {
  if (searchIsOn == false) {
    searchIsOn = true

    document
      .querySelector('#search')
      .classList
      .add('active')
    
  } else {
    searchIsOn = false

    document
    .querySelector('#search')
    .classList
    .remove('active')
  }
}